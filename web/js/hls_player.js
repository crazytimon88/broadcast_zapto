var resolutions = [
  {
    'id': 0,
    'title': 240,
  },
  {
    'id': 1,
    'title': 360,
  },
  {
    'id': 2,
    'title': 480,
  },
  {
    'id': 3,
    'title': 720,
  },
  {
    'id': 4,
    'title': 1080,
  },
]
/* Get the element you want displayed in fullscreen mode (a video in this example): */
function openFullscreen(videoElement) {
  if (videoElement.requestFullscreen) {
    // videoElement.requestFullscreen();
    videoElement.parentNode.requestFullscreen();
  } else if (videoElement.mozRequestFullScreen) { /* Firefox */
    videoElement.mozRequestFullScreen();
  } else if (videoElement.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    // videoElement.webkitRequestFullscreen();
    element.parentNode.webkitRequestFullScreen();
  } else if (videoElement.msRequestFullscreen) { /* IE/Edge */
    videoElement.msRequestFullscreen();
  }
  // 'fullscreen'
}

function enterFullScreen(id) {
  var element = document.getElementById(id);
  element.parentNode.webkitRequestFullScreen();
  element.style.height = screen.height;
  element.style.width = screen.width;
}
//

if (Hls.isSupported()) {
  var controls = $('.controls')[0];
  var video = document.getElementById('video');
  hls = new Hls();
  // hls.loadSource('http://35.228.8.138/hls/test.m3u8');
  hls.loadSource('http://broadcast.zapto.org/hls/test.m3u8');
  hls.attachMedia(video);

  let getResolutionById = function(searchedId) {
    return resolutions.find(function(x) {
      return x.id == searchedId;
    })
  }

  hls.on(Hls.Events.LEVEL_LOADED, function(e, e1, e2) {
    let currentResolutionText = getResolutionById(e1.level);
    $('.js-resolution-current').text(
      currentResolutionText.title
    );
    video.removeAttribute('controls');
  });

  hls.on(Hls.Events.MANIFEST_PARSED, function() {
    video.play();
  });

  $('.js-resolution-value').on('click', function(e) {
    hls.currentLevel = parseInt(e.currentTarget.dataset.id);
  });

  $('.js-sound-btn').on('click', function(e) {
    video.muted = !video.muted;
  });

  $('.js-fullscreen-button').on('click', function() {
    if (document.fullscreen) {
      document.exitFullscreen();
    } else {
      openFullscreen(video);
    }
    video.removeAttribute("controls");
  });

  document.addEventListener("fullscreenchange", function () {
    if (document.fullscreen) {
      video.style.width = '100%';
      video.style.height = '100%';
      controls.classList.add('fullscreen');
    } else {
      controls.classList.remove('fullscreen');
    }
  });
} else {
  alert('Hls is not supported!!!');
}
